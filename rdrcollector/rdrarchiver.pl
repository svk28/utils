#!/usr/bin/perl

#########################################################
# Скрипт для архивирования файлов созданных rdrcollector
#
# вызывается командой:
# rdrarchiver.pl /usr/local/etc/rdrcollector.conf
# 
# Запускается по крону с требуемой периодичностью (сутки)
#
# Автор: Сергей Калинин e-mail: banzaj28@yandex.ru
# распространяется под лицензией GPL
# (c) 2015
#########################################################

use IO::File;
use Getopt::Long;
use File::Path qw(make_path);

my $opts_pair = IO::File->new("$ARGV[0]") or
die "Can not open: $opts{'conf'} $!\n";

while (<$opts_pair>)
{
    ($key, $value) = split('=');
    $value =~ s/^\s+|\s+$//g;
    $opts{$key} = $value;
    #print $opts{'source-port'};
}
$opts_pair->close();

# определяем текущую системную дату
my $date = ` /bin/date "+%Y %m %d %H %M %S"`;
my ($cur_year,$cur_month,$cur_day,$cur_hour,$cur_min,$cur_sec) = split('\s',$date);
        
#print "$cur_year,$cur_month,$cur_day,$cur_hour,$cur_min,$cur_sec";
            

# ищем файлы старше 1 дня и архивируем
#$arch = '/bin/find '.$opts{'out-dir'}.' -type f -mtime +1 -exec gzip {} \; -print > rdrcollector.log';
#print "$arch\n";
#my $result = `$arch`;

# Получаем список заархивированных файлов
opendir(DIR, "$opts{'out-dir'}");
my @files = grep('*',readdir(DIR));

#print files;
#my @files = grep(/\.gz/,readdir(DIR));
closedir(DIR);

# создаем каталог для соответствующих сжатых файлов
foreach my $file (@files)
{
    #print $file;
    # в зависимости от имени файла определяем и создаём каталог куда его перемещать
    my $year = substr($file,0,4);
    my $month = substr($file,4,2);
    my $day = substr($file,6,2);
    
    #print "$year $month $day - $cur_day\n";
    
    if ($day ne $cur_day) {
        $dir = $opts{'archive-dir'}."/".$year."/".$month."/".$day;
	make_path $dir;
	my $cmd = '/usr/bin/gzip '.$opts{'out-dir'}.'/'.$file;
	#print $cmd."\n";
	my $result = `$cmd`;
	#print $result;
    # Перемещаем файл в соответствующий каталог
        $move = '/bin/mv '.$opts{'out-dir'}.'/'.$file.'.gz '. $dir.' > rdrcollector.log';
	print "$move\n";
        my $mvResult = `$move`;
    }
    
} 
