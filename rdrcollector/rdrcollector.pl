#!/usr/bin/perl

########################################################
# Сервис сбора RDR потока с CISCO SCE
# Принимает RDR-данные и формирует текстовые файлы
#
# Автор: Сергей Калинин e-mail: banzaj28@yandex.ru
# распространяется под лицензией GPL
# (c) 2015
#########################################################

use strict;
use RDR::Collector;
use IO::File;
use Switch;
use Getopt::Long;

use vars qw(%opts);

my $help = 0;
my $verbose = 0;
my $conf_file;
my $key;
my $value;

GetOptions(\%opts, 'conf=s', 'source-ip=s', 'source-port=s', 'out-dir=s', 'help', 'verbose');

if (defined($opts{'help'}))
{
    print STDERR <<EOT;


 Сервис сбора RDR потока с CISCO SCE

 Принимает RDR-данные и формирует текстовые файлы

 Автор: Сергей Калинин e-mail:banzaj28\@yandex.ru (c) 2015
 Распространяется под GNU Public License


использование:  rdrcollector.pl --conf ConfigFile 
		rdrcollector.pl --source-ip SourceIP --source-port SourcePort --out-dir OutDir

		--conf - Файл конфигурации
		или
		--source-ip - Адрес интерфейса на котором требуется принимать поток данных
		--source-port - номер порта для приёмки потока данных
		--out-dir - каталог куда складывать файлы
		--help - подсказка
		--verbose - вывод полученных данных в консоль

EOT
exit 1;
}


my $opts_pair = IO::File->new("$opts{'conf'}") or
die "Can not open: $opts{'conf'} $!\n";
while (<$opts_pair>)
{
    ($key, $value) = split('=');
    $value =~ s/^\s+|\s+$//g;
    $opts{$key} = $value;
    #print $opts{'source-port'};
}
$opts_pair->close();

print "ServerIP = $opts{'source-ip'} \nServerPort = $opts{'source-port'}\nOut dir = $opts{'out-dir'}\n";

my $rdr_client = new RDR::Collector(
                    [
                        ServerIP => $opts{'source-ip'},
                        ServerPort => $opts{'source-port'},
                        Timeout => 2,
                        DataHandler => \&display_data
                        ]
                        );

    # Setup the local RDR listener
    my $status = $rdr_client->connect();

    # If we could not listen tell us why.
    if ( !$status )
        {
        print "Status was '".$rdr_client->return_status()."'\n";
        print "Error was '".$rdr_client->return_error()."'\n";
        exit(0);
        }
    # Now just wait for RDR data.
    $rdr_client->check_data_available();
    exit(0);

    # This routine is called from DataHandler when the module
    # instance is initialised.
    # 4 parameters are returned, internal ref, remote IP, remote Port and
    # the raw data

sub display_data
{
    my ( $glob ) = shift;
    my ( $remote_ip ) = shift;
    my ( $remote_port ) = shift;
    my ( $data ) = shift;
    my $attribute_line;
    my $data_line;
    my @keys = keys %{$data};
    # каталог для работы 
    my $workDir = "$opts{'out-dir'}";
    #определяем текущую даты и время
    my $date = ` /bin/date "+%Y %m %d %H %M %S"`;
    my ($year,$month,$day,$hour,$min,$sec) = split('\s',$date);

    #rint "$year,$month,$day,$hour,$min,$sec";

    my $outString;
    my $date_time;
    my $subscriber_id;
    my $client_ip;
    my $client_port;
    my $server_ip;
    my $server_port;
    my $up;
    my $down;
    my $info_string;
    my $protocol_signature;
    my $fileName;
    # имя выходного файла, вида /home/svk/tmp/2015/2/26/201502260929
#    my $fileName = $workDir."/".$year."".$month."".$day."".$hour."".$min;
    # выводим название файла в консоль
#    if ( $opts{'verbose'} ) { print "print $fileName;\n"; }
    # выдёргиваем нужные нам поля и преобразуем по необходимости
    foreach my $key_name ( @keys )
        {
	    #print $key_name." - ".${$data}{$key_name};
 
    	    $outString = "";
    	    # преобразуем время из unixtime в нормальный вид
            if ($key_name eq "report_time" || $key_name eq "end_time")
	    {
		    #if (${$data}{$key_name} eq "") {return; }
		    #print ${$data}{$key_name}."\n";
		    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime( ${$data}{$key_name} );
		    $year += 1900;
		    $mon  += 1;
		    if ($mon < 10)
		    {
			$mon = "0".$mon;
		    }
		    if ($mday < 10)
		    {
			$mday = "0".$mday;
		    }
		    if ($hour < 10)
		    {
		    	$hour = "0".$hour;
		    }
		    if ($min < 10)
		    {
			$min = "0".$min;
		    }
		    if ($sec < 10)
		    {
			$sec = "0".$sec
		    }
    		$date_time = "$hour:$min:$sec $mday.$mon.$year";
	        # имя выходного файла, вида /home/svk/tmp/2015/2/26/201502260929
		$fileName = $workDir."/".$year."".$mon."".$mday."".$hour."".$min;
	        if ( $opts{'verbose'} ) { print "print $fileName;\n"; }
    	    } else {
    		if ( $opts{'verbose'} ) { print "$key_name - ${$data}{$key_name}\n"; }
	    }
            switch ($key_name) {
		    case "flavour_id" {
        	    }
    		    case "protocol_signature" {
    			$protocol_signature = ${$data}{$key_name};
    		    }
		    case "subscriber_id" {
    			$subscriber_id = ${$data}{$key_name};
    		    }
        	    case "client_ip" {
			$client_ip = ${$data}{$key_name};
    		    }
        	    case "client_port" {
			$client_port = ${$data}{$key_name};
    		    }
        	    case "server_ip" {
			$server_ip  = ${$data}{$key_name};
    		    }
        	    case "server_port" {
			$server_port = ${$data}{$key_name};
    		    }
        	    case "info_string" {
			$info_string = ${$data}{$key_name};
    		    }
        	    case "session_downstream_volume" {
			$down = ${$data}{$key_name};
    		    }
    		    case "session_upstream_volume" {
    			$up = ${$data}{$key_name};
    		    }

    	    }
    }
        # создаём файл и открываем его на запись
        if ($fileName ne "") {
            #print "$fileName\n";
	    open(fileOut,">> $fileName") or die "cannot open $fileName: $!";
	
	
            # формируем результат и выводим в файл и в консоль если указана опция --verbose
	    $outString = $date_time."\t".$subscriber_id."\t".$client_ip.":".$client_port."\t".$server_ip.":".$server_port."\t".$up."/".$down."\t".$protocol_signature."\t".$info_string;
	    print fileOut $outString."\n";
	    if ( $opts{'verbose'} ) { print "\n"; }
	    close fileOut;
	}
}


