#!/usr/bin/perl
#########################################
#
#      ElasticSearch index remover
#
# Author: Sergey Kalinin
#
#########################################

use Getopt::Long;

use vars qw(%opts);
use feature qw(say);


my $help = 0;
my $debug = 0;
my $domain_file;

GetOptions(\%opts, 'host=s', 'days=s', 'data-type=s', 'help', 'verbose');

if (defined($opts{'help'}))
{
    print STDERR <<EOT;
    ElastickSearch index remover
    
    Usage: $0 --host localhost --days (days) --data-type type-of-data [--verbose --help]
    
    --host - elasticsearch(kibana) host name or ip address
    --days - days ago count
	--data-type - must be like kibana index (netflow, log4j, logback, etc)
    --verbose - output debug information
    --help - print this message
    
EOT
exit 1;
}

if ( $opts{'host'} eq "" ) {
    print "Wrong host: $opts{'host'}\n";
    exit;
} else {
	$host = $opts{'host'};
}

if ( $opts{'days'} eq "" || $opts{'days'} !~ /^\d+$/ ) {
    print "Wrong days count: $opts{'days'}\n";
    exit;
} else {
	$days = $opts{'days'};
}

if ( $opts{'data-type'} eq "" ) {
    print "Wrong data type: $opts{'data-types'}\n";
    exit;
} else {
    $data_type = $opts{'data-type'};
}

$date = `date --date="-$days day" +%Y.%m.%d`;
$date =~ s/\s//g;

# get neflow index from kibana
#say "curl -s -XGET \"http://$host:9200/_cat/indices?v&pretty\" -H 'Content-Type: application/json' | grep $data_type";
my $data_list = `curl -s -XGET "http://$host:9200/_cat/indices?v&pretty" -H 'Content-Type: application/json' | grep $data_type`;

foreach my $index (split('\n',$data_list)) {
	#$index =~ /netflow-(\d{4}\.\d{2}\.\d{2})/;
	$index =~ /$data_type-(\d{4}\.\d{2}\.\d{2})/;
	$index_date = $1;
 
	if ($index_date eq $date) {
		$del_cmd = "curl -s -XDELETE 'http://$host:9200/$data_type-$index_date' -H 'Content-Type: application/json'";
		say $del_cmd;
		$res = `$del_cmd`;
		say "Result: $res" if $res;
	}
}
