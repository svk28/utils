# My some utils

switch_conf_backup - Скрипт на perl, для резервного копирования конфигурации коммутаторов d-link и edgecore. Работает в связке с GLPI.

rdrcollector - Сервис сбора RDR (Raw Data Records) потока с CISCO SCE и cgi-скрипты для визуализации.

elasticsearch_index_del - Скрипт на perl, позволяющий удалять любые заданные индексы в elastic не позднее определенного количества дней.